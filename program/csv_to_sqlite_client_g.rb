require 'csv'
require 'sqlite3'
require "gnuplot"

def rbb (z2=2.2, tt=5, e=15, r=5.0, z=100, hash_array=@hash_array)
  @aaa = []
  hash_array.each do |i|
    t = {}
    t[:time] = i[:time]
    t[:gyz] = i[:gyz]
    t[:section] = i[:gyz].to_i > z || i[:gyz].to_i < -1*z ? 10 : 0
    @aaa << t
  end
  bbb = []
  queue = []
  @aaa.each do |i|
    if queue.size > e
      if r < (queue.inject(0.0){|r,o| r+=o[:section] }/e)
        puts (queue.inject(0.0){|r,o| r+=o[:section] }/e)
        puts bbb[bbb.size-e][:time]+"~"+i[:time]
        (1..e).each do |q|
          puts bbb.size-q
          bbb[bbb.size-q][:section] = 20
        end
        queue = []
      else
        queue.pop
      end
    end
    queue << i
    bbb << i
  end
  @aaa = bbb
  
  tt.times do
    bbb = []
    mprev = @aaa.first[:section]
    @aaa.each do |i|
      bbb << i
      bbb[bbb.size-1][:section] = mprev * 0.8 + bbb[bbb.size-1][:section] * 0.2
      mprev = i[:section]
    end
    @aaa = bbb
  end
  
  bbb = []
  @aaa.each do |i|
    t = {}
    t[:time] = i[:time]
    t[:gyz] = i[:gyz]
    t[:section] = i[:section].to_i > z2 ? 10 : 0
    bbb << t
  end
  @aaa = bbb
end

def readcsv(path="/Users/nokkii/Desktop/1.csv")
  #cnt = 0
  #記録日
  array = []
  CSV.foreach(path) do |row|
    hash = {}
    id, gx, gy, gz, gyx, gyy, gyz, time = row
    puts time
    if row[0] != "id"
      #hash[:id] = cnt
      hash[:time] = time
      hash[:gyz] = gyz
      array << hash
    end
    #cnt+=1
  end
  array
end
@hash_array = readcsv

def writedb(array=@aaa, path="/Users/nokkii/Dropbox/t.db")
  #FileUtils.rm_r("/Users/nokkii/Dropbox/t.db", :secure => true)
  db = SQLite3::Database.new(path)
  sql = <<-SQL
  CREATE TABLE calibrationData (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    time TEXT,
    gyz REAL,
    section INTEGER,
    count INTEGER
    );
  SQL
  db.execute(sql)
  
  sqls = ""
  array.each do |hash|
    sqls += "insert into calibrationData(time, gyz, section, count) values('#{hash[:time]}',#{hash[:gyz]},#{hash[:section]},#{hash[:count].nil? ? 0 : hash[:count]});"
  end
  db.execute_batch(sqls)
end
writedb

def readdb(path="./ana.db")
  db = SQLite3::Database.new(path)
  sql = <<-SQL
    SELECT * FROM calibrationData;
  SQL
  hash_array = []
  db.execute(sql).each do |array|
    hash = {}
    hash[:time] = array[1]
    hash[:gyz] = array[2]
    hash_array << hash
  end
  hash_array
end

def aaa
  @a.each 
end

def split_data_with_sec(array=@hash_array)
  before_h = nil
  tmp_array = []
  all = []
  array.each_with_index do |h, i|
    puts i if 0 == i % 10000
    unless before_h.nil?
      if 2.0 < Time.parse(h[:time]) - before_h
        all << tmp_array
        tmp_array = []
      end
    end
    tmp_array << h
    before_h = Time.parse(h[:time])
  end
  minite = []
  before_h = nil
  all.each do |a|
    minite << [{}] if 3 * 60.0 < Time.parse(a.first[:time]) - before_h unless before_h.nil?
    minite << a if 20 < a.count
    before_h = Time.parse(a.first[:time])
  end
  minite
end

def print(splitted_datas=@splitted_datas, threshold=300)
  splitted_datas.each_with_index do |s, i|
    if 10 < s.count
      up_ = nil
      up_down_count = 0
      unless s.first[:section].nil?
        s.each do |d|
          if ( up_ != (d[:gyz]>=0) || up_ == nil ) && (d[:section]!=0) && (d[:gyz].abs > threshold)
            up_ = (d[:gyz]>=0)
            up_down_count += 1
          end
        end
        s.first[:count] = up_down_count
      end
    end
    puts i.to_s + " " + s.first[:time].to_s + " " + s.count.to_s + "       " + s.first[:count].to_s + "       " + (s.first[:count] > 3 ? "○" : "×" unless s.first[:section].nil?).to_s
  end
  nil
end



def datas_rbb(hash_datas)
  datas = []
  hash_datas.each do |d|
    if 10 > d.each.count
      datas << d
    else
      datas << rbb(2.2, 5, 15, 5.0, 100, d)
    end
  end
  datas
end

def plot(hash_array)
  Gnuplot.open do |gp|
    Gnuplot::Plot.new( gp ) do |plot|
      plot.title  'Angular velocity Z-axis'
      plot.ylabel 'Angular velocity[rad/s]'
      plot.xlabel 'Time(s)'
      plot.xzeroaxis
      plot.yrange '[-600:600]'
      plot.terminal("png size 2000, 900")
      plot.output("y_eq_x2.png")
  
      x = hash_array.map {|hash| hash[:gyz] }
      t = Time.parse(hash_array.first[:time])
      y = hash_array.map {|hash| Time.parse(hash[:time]) - t }
  
      plot.data << Gnuplot::DataSet.new( [y, x] ) do |ds|
        ds.with = "lines"
        ds.notitle
      end
    end
  end
end


def plot_array(hash_array_array, name="a")
  hash_array_array.each_with_index do |hash_array, i|
    Gnuplot.open do |gp|
      Gnuplot::Plot.new( gp ) do |plot|
        plot.title  'Angular velocity Z-axis'
        plot.ylabel 'Angular velocity[rad/s]'
        plot.xlabel 'Time(s)'
        plot.xzeroaxis
        plot.yrange '[-600:600]'
        plot.terminal("png size 2000, 900")
        plot.output(name + i.to_s + ".png")
    
        x = hash_array.map {|hash| hash[:gyz] }
        t = Time.parse(hash_array.first[:time])
        y = hash_array.map {|hash| Time.parse(hash[:time]) - t }
    
        plot.data << Gnuplot::DataSet.new( [y, x] ) do |ds|
          ds.with = "lines"
          ds.notitle
        end
      end
    end
  end
end

Gnuplot.open do |gp|
	Gnuplot::Plot.new( gp ) do |plot|
		plot.title  'test'
		plot.ylabel 'ylabel'
		plot.xlabel 'xlabel'
    plot.terminal("png")
    plot.output("y_eq_x2.png")
 
		x = (-100..100).collect {|v| v.to_f}
		y = (-100..100).collect {|v| v.to_f ** 2}
 
		plot.data << Gnuplot::DataSet.new( [x, y] ) do |ds|
			ds.with = "lines"
			ds.notitle
		end
	end
end

# 使い方

# データベースから読み込んでしきい値を適応して新しいDBに書き込む
@hash_array = readdb
rbb
writedb


# データベースから読み込んで、数秒の間隔が空いているデータは別々の配列に格納、それぞれをしきい値に分割、ピークの数をカウントしてわだち判定、センサのグラフを出力
@hash_array = readdb("./ana.db")
@splitted_datas = split_data_with_sec
rbb = datas_rbb(@splitted_datas)
print(rbb, 300)
plot(rbb[49])
