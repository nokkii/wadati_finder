#!/bin/sh

FILE_NAME=master
LATEX=/Library/TeX/texbin/platex
BIBTEX=/Library/TeX/texbin/pbibtex
DVIPDF=/Library/TeX/texbin/dvipdfmx

$LATEX $FILE_NAME &&\
$BIBTEX $FILE_NAME &&\
$LATEX $FILE_NAME &&\
$LATEX $FILE_NAME &&\
$DVIPDF $FILE_NAME
