# 卒論・修論・博論のスタイルファイル

Bookスタイル、12pt、いろいろな便利マクロ付き

## 概要

このテンプレートは

ssh://sub-www.ubi.cs.ritsumei.ac.jp//var/hg_repos/vori/Thesis_pLatex_Template

および

<http://www.ubi.cs.ritsumei.ac.jp/inner/pukiwiki/?%C2%B4%CF%C0%A1%A6%BD%A4%CF%C0%A1%A6%C7%EE%CF%C0%A4%CE%A5%B9%A5%BF%A5%A4%A5%EB%A5%D5%A5%A1%A5%A4%A5%EB>

<http://www.ubi.cs.ritsumei.ac.jp/inner/pukiwiki/?%C2%B4%B6%C8%CF%C0%CA%B8>

に添付の

Thesis pLaTeX Template vori_style.zip

Thesis_pLatex_Template.zip

Thesis_pLatex_Template_UTF8 2.zip

を改変したものです。